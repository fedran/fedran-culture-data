Fedran Culture Data
===================

This is a collection of JSON files that represent various elements of the [Fedran](https://fedran.com/) fantasy world created by [D. Moonfire](https://d.moonfire.us/).

# Data Files

All of the data files are available in expanded and minified versions in the `./dist/` directory.

# JavaScript Access

The components of the data library are available with `require`.

```javascript
var data = require("mfgames-culture-data");

// Display the internal directory for the source files.
console.log(data.dataDirectory);

// Combines an object that represents the combined.min.js.
console.log(data.combined);

// Contains the index entries (index.min.js).
console.log(data.index);
```
