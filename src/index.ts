import * as path from "path";

const indexPath = require.resolve("./index");
const libDir = path.dirname(indexPath);
const rootDir = path.dirname(libDir);

export const dataDirectory = path.join(rootDir, "dist");
export const combined = require(path.join(dataDirectory, "combined.json"));
export const index = require(path.join(dataDirectory, "index.json"));
