import * as expect from "expect";
import * as mfgamesCulture from "mfgames-culture";
import "mocha";
import * as path from "path";
import * as fedranCulture from "../index";

// Load the culture, we only need one of them for this.
let combined = fedranCulture.combined;
var provider = new mfgamesCulture.PropertyCultureDataProvider(combined);
var cultureData = provider.getCultureSync("kyo");
var culture = new mfgamesCulture.Culture(cultureData);

// We have a common pattern for these tests, so define a function based on the
// name of the test.
function run(done)
{
    try
    {
        let [input, format, expected] = this.test.title.split(" | ");
        let instant = culture.parseInstant(input);
        let results = culture.formatInstant(instant, format);

        expect(results).toEqual(expected);
        done();
    }
    catch (exception)
    {
        done(exception);
    }
}

// Run the various round-trip tests.
describe(path.basename(__filename), function()
{
    it("1454/7/41 MTR | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 1::1:1", run);
    it("1454/7/41 MTR 1:: | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 1::1:1", run);
    it("1454/7/41 MTR 1::1 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 1::1:1", run);
    it("1454/7/41 MTR 1::1:1 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 1::1:1", run);

    it("1454/7/41 MTR | YYYY/R/O MTR | 1454/7/41 MTR", run);
    it("1454/7/41 MTR 1:: | YYYY/R/O MTR | 1454/7/41 MTR", run);
    it("1454/7/41 MTR 1::1 | YYYY/R/O MTR | 1454/7/41 MTR", run);
    it("1454/7/41 MTR 1::1:1 | YYYY/R/O MTR | 1454/7/41 MTR", run);

    it("1454/7/41 MTR 2:: | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 2::1:1", run);
    it("1454/7/41 MTR 3:: | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 3::1:1", run);
    it("1454/7/41 MTR 4:: | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 4::1:1", run);
    it("1454/7/41 MTR 5:: | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 5::1:1", run);

    it("1454/7/41 MTR 2::2 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 2::2:1", run);
    it("1454/7/41 MTR 3::3 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 3::3:1", run);
    it("1454/7/41 MTR 4::4 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 4::4:1", run);
    it("1454/7/41 MTR 5::5 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 5::5:1", run);

    it("1454/7/41 MTR 2::2:2 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 2::2:2", run);
    it("1454/7/41 MTR 3::3:3 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 3::3:3", run);
    it("1454/7/41 MTR 4::4:4 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 4::4:4", run);
    it("1454/7/41 MTR 5::5:5 | YYYY/R/O MTR K::M:B | 1454/7/41 MTR 5::5:5", run);
});
