import * as expect from "expect";
import * as mfgamesCulture from "mfgames-culture";
import "mocha";
import * as path from "path";
import * as fedranCulture from "../index";

// Load the culture, we only need one of them for this.
let combined = fedranCulture.combined;
var provider = new mfgamesCulture.PropertyCultureDataProvider(combined);
var cultureData = provider.getCultureSync("kyo");
var culture = new mfgamesCulture.Culture(cultureData);

// We have a common pattern for these tests, so define a function based on the
// name of the test.
function run(done)
{
    try
    {
        let [input, format, expected] = this.test.title.split(" | ");
        let period = culture.parsePeriod(input);
        let results = culture.formatPeriod(period, format);

        expect(results).toEqual(expected);
        done();
    }
    catch (exception)
    {
        done(exception);
    }
}

// Run the various round-trip tests.
describe(path.basename(__filename), function()
{
    it("0:: | K::M:B | 0::0:0", run);
    it("0::0 | K::M:B | 0::0:0", run);
    it("0::0:0 | K::M:B | 0::0:0", run);

    it("1:: | K::M:B | 1::0:0", run);
    it("1::1 | K::M:B | 1::1:0", run);
    it("1::1:1 | K::M:B | 1::1:1", run);
});
