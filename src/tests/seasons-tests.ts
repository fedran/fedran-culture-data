import * as expect from "expect";
import { PropertyCultureDataProvider, Culture } from "mfgames-culture";
import * as cultureData from "mfgames-culture-data";
import "mocha";
import * as path from "path";
import * as fedranCulture from "../index";

// Load the culture, we only need one of them for this.
let usProvider = new PropertyCultureDataProvider(cultureData.combined);
let usData = usProvider.getCultureSync("en-US");
let us = new Culture(usData);
let combined = fedranCulture.combined;
var provider = new PropertyCultureDataProvider(combined);
var tarData = provider.getCultureSync("tar");
var tar = new Culture(tarData);
var natData = provider.getCultureSync("nat");
var nat = new Culture(natData);

// We have a common pattern for these tests, so define a function based on the
// name of the test.
function run(done: (ex?: any) => void)
{
    try
    {
        let [tarInput, northBigOutput, northTrimester, northSmallOutput] = this.test.title
            .split(" | ")
            .map((t: string) => t.trim());
        let tarInstant = tar.parseInstant(tarInput);
        let natInstant = nat.parseInstant(tarInstant);
        let northBigResult = nat.formatInstant(natInstant, "northernBigSeasons");
        let trimesterResult = nat.formatInstant(natInstant, "northernBigSeasonTrimesters");
        let northSmallResult = nat.formatInstant(natInstant, "northernSmallSeasons");

        expect(northBigResult).toEqual(northBigOutput);
        expect(northTrimester).toEqual(trimesterResult);
        expect(northSmallResult).toEqual(northSmallOutput);
        done();
    }
    catch (exception)
    {
        done(exception);
    }
}

// Run the various round-trip tests.
describe(path.basename(__filename), function()
{
    it(" 0.0.1858 TSC | Spring | Early Spring | Start of spring", run);
    it("13.0.1858 TSC | Spring | Early Spring | Rain waters", run);
    it("30.0.1858 TSC | Spring | Mid-Spring   | Going-out of the worms", run);
    it("45.0.1858 TSC | Spring | Mid-Spring   | Vernal equinox", run);
    it("11.1.1858 TSC | Spring | Late Spring  | Clear and bright", run);
    it("26.1.1858 TSC | Spring | Late Spring  | Rain for harvests", run);
    it("42.1.1858 TSC | Summer | Early Summer | Start of summer", run);
    it(" 9.2.1858 TSC | Summer | Early Summer | Small blooming", run);
    it("25.2.1858 TSC | Summer | Mid-Summer   | Seeds and cereals", run);
    it("41.2.1858 TSC | Summer | Mid-Summer   | Reaching summer", run);
    it(" 5.3.1858 TSC | Summer | Late Summer  | Small heat", run);
    it("20.3.1858 TSC | Summer | Late Summer  | Big heat", run);
    it("36.3.1858 TSC | Autumn | Early Autumn | Start of autumn", run);
    it(" 1.4.1858 TSC | Autumn | Early Autumn | Lessening heat", run);
    it("16.4.1858 TSC | Autumn | Mid-Autumn   | White dew", run);
    it("32.4.1858 TSC | Autumn | Mid-Autumn   | Autumnal equinox", run);
    it("47.4.1858 TSC | Autumn | Late Autumn  | Cold dew", run);
    it("11.5.1858 TSC | Autumn | Late Autumn  | Frosting", run);
    it("25.5.1858 TSC | Winter | Early Winter | Start of winter", run);
    it("40.5.1858 TSC | Winter | Early Winter | Small snow", run);
    it(" 4.6.1858 TSC | Winter | Mid-Winter   | Big snow", run);
    it("23.6.1857 TSC | Winter | Mid-Winter   | Winter solstice", run);
    it("37.6.1857 TSC | Winter | Late Winter  | Small cold", run);
    it(" 1.7.1857 TSC | Winter | Late Winter  | Big cold", run);
});
