import * as expect from "expect";
import * as mfgamesCulture from "mfgames-culture";
import "mocha";
import * as path from "path";
import * as fedranCulture from "../index";

// Load the culture, we only need one of them for this.
let combined = fedranCulture.combined;
var provider = new mfgamesCulture.PropertyCultureDataProvider(combined);
var cultureData = provider.getCultureSync("kyo");
var culture = new mfgamesCulture.Culture(cultureData);

// We have a common pattern for these tests, so define a function based on the
// name of the test.
function run(done: (ex?: any) => void)
{
    try
    {
        let [instantInput, periodInput, expected] = this.test.title.split(" | ");
        let instant = culture.parseInstant(instantInput);
        let period = culture.parsePeriod(periodInput);
        let last = culture.getLast(instant, period);
        let results = culture.formatInstant(last, "YYYY/R/O MTR K::M:B");

        expect(results).toEqual(expected);
        done();
    }
    catch (exception)
    {
        done(exception);
    }
}

// Run the various round-trip tests.
describe(path.basename(__filename), function()
{
    it("1454/7/41 MTR | 0:: | 1454/7/41 MTR 1::1:1", run);
    it("1454/7/41 MTR | 1:: | 1454/7/41 MTR 2::1:1", run);
    it("1454/7/41 MTR | 2:: | 1454/7/41 MTR 3::1:1", run);

    it("1454/7/41 MTR | 0::0 | 1454/7/41 MTR 1::1:1", run);
    it("1454/7/41 MTR | 0::1 | 1454/7/41 MTR 1::2:1", run);
    it("1454/7/41 MTR | 0::2 | 1454/7/41 MTR 1::3:1", run);
});
